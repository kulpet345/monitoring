FROM python:3.8

RUN apt-get update

COPY requirements.txt .

RUN pip3 install -r requirements.txt 

COPY main.py .
COPY history.json ./

CMD ["python", "main.py"]
