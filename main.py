import requests
import numpy as np
import pandas as pd
from datetime import datetime
import graphyte
import json


def load_history(start_date: str, end_date: str):
    dates = pd.date_range(start_date, end_date)
    history = []
    for date in dates:
        date_s = date.strftime("%Y-%m-%d")
        print(date_s)
        date_f = (date + pd.Timedelta("1d")).strftime("%Y-%m-%d")
        resp = requests.get(
            "https://api.weatherbit.io/v2.0/history/daily",
            params={"city": "Moscow", "start_date": date_s, "end_date": date_f, "key": "03b13f0172984a1bb4256632933aa800"}
        ).json()
        history.append(resp["data"][0])
    return history


history = load_history("2020-05-01", "2020-05-23")

with open("history.json") as f:
    history = json.load(f)

sender = graphyte.Sender("graphite")
for entry in history:
    sender.send("weather.temp", entry["temp"], timestamp=entry["ts"])
    sender.send("weather.precip", entry["precip"], timestamp=entry["ts"])
